<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id'=> 1,
			'first_name' => 'Vuong',
			'last_name' => 'Manh',
			'email' =>  'manhlong29590@gmail.com',
			'password' => bcrypt('123456'),
			'status' => "active",
			'created_at'    =>  Carbon::now(),
			'updated_at'    =>  Carbon::now(),

		]);
    }
}

