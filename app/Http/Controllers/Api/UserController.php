<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::withTrashed()->paginate();
        return [
            "status" => "success",
            "data" => $users
        ];
    }

    public function store(UserRequest $request)
    {
        try {
            $user = User::create($request->all());
            return response()->json(["status"=> "success", "data"=> $user], 201);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function update(UserRequest $request, $id)
    {
        try {
            $user = User::findOrFail($id);
            $user->update($request->all());
            return response()->json(["status"=> "success", "data"=> $user], 200);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function delete($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->status = "deleted";
            $user->save();
            $user->delete();
            return response()->json(["status"=> "success"], 200);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }

    }
}
