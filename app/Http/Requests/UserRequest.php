<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'string', 'min:2', 'max:30'],
            'last_name' => ['required', 'string', 'min:2', 'max:30'],
            'email' => ['required', 'email', 'unique:users,email,'.$this->id, 'string', 'min:2', 'max:50'],
            'password' => ['nullable', 'string', 'min:8', 'max:50'],
            'status' => ['required', Rule::in(['active', 'suspended', 'deleted']) ],
        ];
    }

    public function messages()
	{
		return [
			'first_name.*'	=>	'Tên có độ dài 2-50 ký tự',
			'last_name.*'	=>	'Tên có độ dài 2-50 ký tự',
			'email.unique'	=>	'Email đã tồn tại',
			'email.email'	=>	'Email không hợp lệ',
            'email'	=>	'Email có độ dài 2-50 ký tự',
			'password'	=>	'Password có độ dài 8-50 ký tự',
            'status.*' => 'Status không hợp lệ',
		];
	}

    protected function failedValidation(Validator $validator)
	{
		$errors = (new \Illuminate\Validation\ValidationException($validator))->errors();
		throw new HttpResponseException(response()->json(
			[
                'status' => 'error',
				'msg' => $errors,
			]));
	}
}
