<?php

use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/users',[UserController::class, 'index'])->name('user.index');
Route::post('/user',[UserController::class, 'store'])->name('user.store');
Route::put('/user/{id}',[UserController::class, 'update'])->name('user.update');
Route::delete('/user/{id}',[UserController::class, 'delete'])->name('user.delete');
